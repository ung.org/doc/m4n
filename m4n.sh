#!/bin/sh
M4N_DIR=.
oformat=text

while getopts f: option; do
	case ${option} in
	f)	oformat="$OPTARG";;
	esac
done

if ! [ -r "${M4N_DIR}/${oformat}.m4n" ]; then
	printf '%s: %s: no such format\n' "$0" "$oformat"
	exit 1
fi

shift $((OPTIND - 1))

m4 "${M4N_DIR}/preamble.m4n" $1 "${M4N_DIR}/synopsis.m4n" "${M4N_DIR}/${oformat}.m4n"
